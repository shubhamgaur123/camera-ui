import React, { useState, useEffect } from "react";
import Webcam from "react-webcam";
import b64toBlob from "b64-to-blob";

function WebcamComponent(props) {
  const buttonStyle = { width: "100px" };
  const videoConstraints = {
    width: 480,
    height: 320,
    facingMode: "user"
  };

  const webcamRef = React.useRef(null);
  const [imgSrc, setimgSrc] = useState("");
  const [imgName, setimgName] = useState("");
  const [uploadImageState, setuploadImageState] = useState(false);

  useEffect(() => {
    navigator.getMedia =
      navigator.getUserMedia || // use the proper vendor prefix
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia;

    navigator.getMedia(
      { video: true },
      function() {
        // webcam is available
       
      },
      function() {
        // webcam is not available
        alert("Webcam is not available");
      }
    );
  }, []);

  const uploadImage = () => {
    setuploadImageState(!uploadImageState);
  };

  const capture = () => {
    setimgSrc(webcamRef.current.getScreenshot());
  };

  const handleSubmit = e => {
    e.preventDefault();
    let block = imgSrc.split(";");
    // Get the content type
    let contentType = block[0].split(":")[1]; // In this case "image/gif"
    // get the real base64 content of the file
    let realData = block[1].split(",")[1]; // In this case "iVBORw0KGg...."

    // Convert to blob
    let blob = b64toBlob(realData, contentType);
    let imageObj = { imgName, imgSrc, blob };
    console.log(imageObj);
    // for reading the blob file
    var reader = new FileReader();
    reader.readAsDataURL(imageObj.blob);
    reader.onloadend = function() {
      var base64data = reader.result;
      console.log("blob to base 64 data", base64data);
    };

    setimgSrc("");
  };

  const handleChange = e => {
    setimgName(e.target.value);
  };

  const retake = () => {
    setimgSrc("");
  };

  return (
    <>
      {!imgSrc && (
        <Webcam
          audio={false}
          height={320}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          width={480}
          screenshotQuality={0.95}
          videoConstraints={videoConstraints}
          style={{ margin: "auto", padding: "20px" }}
        />
      )}
      {imgSrc ? (
        ""
      ) : (
        <button onClick={capture} style={buttonStyle}>
          Capture photo
        </button>
      )}
      <hr></hr>
      {imgSrc && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          <img
            src={imgSrc}
            style={{ width: 480, height: 320, padding: "20px" }}
            alt=""
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              width: "480px",
              padding: "10px"
            }}
          >
            <button onClick={retake} style={buttonStyle}>
              Retake
            </button>
            <button onClick={uploadImage} style={buttonStyle}>
              Upload
            </button>
          </div>
          {uploadImageState && (
            <>
              <form onSubmit={handleSubmit}>
                <input
                  type="text"
                  name="imgName"
                  value={imgName}
                  onChange={handleChange}
                />
                <input type="submit" value="save" />
              </form>
            </>
          )}
        </div>
      )}
    </>
  );
}

export default WebcamComponent;
