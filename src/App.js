import React from "react";
import Webcam from "./components/webcam";

function App() {
  return (
    <div
      className="App"
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <h1 style={{ textAlign: "center" }}>Camera</h1>
      <Webcam />
    </div>
  );
}

export default App;
